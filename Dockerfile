FROM debian:bookworm

ARG USER_ID=1000
ARG GROUP_ID=1000

ENV FTP_USER **String**
ENV FTP_PASS **Random**
ENV PASV_ADDRESS **IPv4**
ENV PASV_ADDR_RESOLVE NO
ENV PASV_ENABLE YES
ENV PASV_MIN_PORT 21100
ENV PASV_MAX_PORT 21110
ENV XFERLOG_STD_FORMAT NO
ENV LOG_STDOUT **Boolean**
ENV FILE_OPEN_MODE 0666
ENV LOCAL_UMASK 077
ENV PASV_PROMISCUOUS NO
ENV PORT_PROMISCUOUS NO

COPY src/entrypoint.bash /entrypoint.bash

RUN chmod +x /entrypoint.bash && \
    apt update -y && \
    apt upgrade -y && \
    apt install -y \
                vsftpd \
                db-util \
                iproute2 && \
    usermod -u ${USER_ID} ftp && \
    groupmod -g ${GROUP_ID} ftp && \
    mkdir -p /home/vsftpd && \
    mkdir -p /etc/vsftpd && \
    mkdir -p /var/run/vsftpd/empty && \
    chown -R ftp:ftp /home/vsftpd && \
    rm /etc/vsftpd.conf

COPY src/vsftpd.conf /etc/vsftpd/vsftpd.conf
COPY src/vsftpd_virtual /etc/pam.d/

ENTRYPOINT [ "/entrypoint.bash" ]